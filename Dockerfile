FROM debian:buster-slim

LABEL maintainer="alex.strokach@utoronto.ca"

# Set an encoding to make things work smoothly
ENV LANGUAGE en_US:en \
    LC_ALL en_US.UTF-8 \
    LANG en_US.UTF-8

RUN apt-get update && apt-get install -y \
        gosu \
        tini \
        git \
        python3.6 \
        python3-pip \
        python3-ipython \
        python3-ipykernel \
        python3-pypandoc \
        python3-nbconvert \
        jupyter-notebook \
        jupyter-nbconvert \
        python3-sphinx \
        python3-sphinx-rtd-theme \
    && rm -rf /var/lib/apt/lists/*

# Set environment variables
ENV USER=myuser \ 
    GROUP=mygroup \
    USER_ID=9001 \
    GROUP_ID=9001 \
    HOME=/home/myuser \
    LOGNAME=myuser \
    MAIL=/var/spool/mail/myuser

# Create user
RUN groupadd -g $GROUP_ID -o $GROUP && \
    useradd --shell /bin/bash -u $USER_ID -g $GROUP_ID -o -c "" -m $USER

# Add entrypoint
COPY entrypoint_source /opt/docker/bin/entrypoint_source
COPY entrypoint /opt/docker/bin/entrypoint

# Ensure that all containers start with tini and the user selected process.
# Provide a default command (`bash`), which will start if the user doesn't specify one.
ENTRYPOINT [ "tini", "--", "/opt/docker/bin/entrypoint" ]
CMD [ "/bin/bash" ]
